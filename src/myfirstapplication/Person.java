/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myfirstapplication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;//class Date is already implemented 

import javax.swing.JTextArea;
import java.lang.String;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
/**
 *
 * @author Gabrysia
 */
public class Person {
    
    private String FirstName;
    private String Surname;
    private IAddress HomeAddress;
    private Date DOB;
    private Date CustomerSince;
    private Date givenDOB;
    private boolean isDateCorrect; //this variables purpose indicates if DOB is the same as givenDOB
     
    //CONSTRUCTORS
    public Person(String line) throws ParseException{
        String[] split = line.split(",");
        FirstName = split[0];// element 0 will be until coma
        Surname = split[1];
        HomeAddress = new IAddress(Integer.parseInt(split[2]),split[3],split[4],split[5],split[6],split[7],split[8]);
        DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss z yyyy", Locale.ENGLISH);
        DOB =  df.parse(split[9]); 
        CustomerSince = df.parse(split[10]);
    }
    
public Person (){
    
    Edit(new String(), new String(), new IAddress(), new Date(), new Date());
    HomeAddress = new IAddress();
    
   
}

public Person (BufferedReader bin){
    
    //(new String(), new String(), new IAddress(), new Date(), new Date(), new Date(), true);
    HomeAddress = new IAddress();
   // this.LoadFromFile(bin);
}

public Person (String FirstName, String Surname, IAddress src, Date DOB, Date CustomerSince){
    
    Edit(FirstName, Surname, src, DOB, CustomerSince);
}



//FILE MANAGEMENT
public String toString(){
    
    return  FirstName + ","+ Surname + ","+ HomeAddress.toString() + "," + DOB + "," + CustomerSince + System.getProperty("line.separator") ;
}


public Date StringtoDate(String string) throws ParseException{
    
    Date date = new SimpleDateFormat("dd/MM/yyyy").parse(string);  
   
    return date;
}

public void Edit (String FirstName, String Surname, IAddress src, Date DOB, Date CustomerSince){

    HomeAddress = src;
    
     this.FirstName = FirstName;
    this.Surname = Surname;
    this.HomeAddress = HomeAddress;
    this.DOB = DOB;
    this.CustomerSince = CustomerSince;
    //this.givenDOB = givenDOB;
    //this.isDateCorrect = isDateCorrect;
    
}
public String Display (){

//      src.append(this.toString());
         String str  =FirstName + "\n" + Surname + "\n"+ getHomeAddress().toString() + "\n" + DOB + "\n" + CustomerSince + "\n";//returns the object and what is inside (methods)
                 return str;
}



public void SaveToFile(FileWriter writer){
    
    try {
            writer.write(toString());
            
            writer.flush();
        } catch (IOException ioe){
            System.out.println("Cannot save");
        }

}



//OTHERS
public void checkDOB(Date DOB, Date givenDOB){
    
    
   if(DOB == givenDOB){
   isDateCorrect = true;
   
   }
   else{
   isDateCorrect =false;
   

   }
    

}

public boolean isDateCorrect(){

    return isDateCorrect;
}

public String getSurname() {
    return Surname;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

public IAddress getHomeAddress(){
    return this.HomeAddress;
}

public Date getDOB (){
    return DOB;
}

public Date getCustomerSince (){
    return CustomerSince;
}



public String getFirstName() {
    return FirstName;
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

//            void LoadFromFile(String line) throws ParseException{
//                 try {
//                    String[] index = line.split(",");
//                    FirstName = index[0];
//                    Surname = index[1];;
//                    HomeAddress = new IAddress(index[2])
//                    HomeAddress.LoadFromFile(bin,false);
//                    DOB = StringtoDate(bin.readLine());
//                    CustomerSince = StringtoDate(bin.readLine());
//                    bin.readLine();//jezeli znajdzie cos co pasuje i przeczyta jeszcze jedna linijke to ogarnie ze to haslo
//
//                }catch(IOException ioe){
//
//                }  
//
//            }

//public String DatetoString (Date date){
//    
//// Create an instance of SimpleDateFormat used for formatting 
//// the string representation of date (d/m/year)
//    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
//
//// Get the date today using Calendar object.
//    date = Calendar.getInstance().getTime();
//// Using DateFormat format method we can create a string 
//// representation of a date with the defined format.
//    String reportDate = df.format(date);
//
//// Print what date is today!
//    System.out.println("Report Date: " + reportDate);
//    return reportDate;
//}
    
   
    }





