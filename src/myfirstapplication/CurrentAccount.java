/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myfirstapplication;

import java.io.BufferedReader;
import java.io.IOException;
import javax.swing.JTextArea;

/**
 *
 * @author Gabrysia
 */
public class CurrentAccount extends Account{
    
    private double Overdraft;
    private String Conditions;
    private double AvailableBalance;
    double WithdrawalFee = 25.00;
    boolean ApplyFee;
   
    private Account AccountDetails;
    double Balance = super.getBalance();
    
    
    public CurrentAccount(){
        Overdraft = 100;
        Conditions = "";
        AvailableBalance = 0.0;
        ApplyFee = false;
        
        
        
    }
    
    public CurrentAccount (String CurrentSortCode, Integer CurrentAccountNumber, Double CurrentBalance, String CurrentBankName, Double CurrentRate, String CurrentLastReportDate, String PrimaryFirstName,
                            String PrimarySurname, Boolean CurrentJoint, String CurrentAccountTypes, Integer CurrentNoTransactions, Double CurrentOverdraft, String CurrentConditions, Double CurrentAvailableBalance){
        
        super(CurrentSortCode, CurrentAccountNumber, CurrentBalance, CurrentBankName, CurrentRate, CurrentLastReportDate, PrimaryFirstName, PrimarySurname, CurrentJoint, CurrentAccountTypes, CurrentNoTransactions);
        Overdraft = CurrentOverdraft;
        Conditions = CurrentConditions;
        AvailableBalance = CurrentAvailableBalance;
        
    }
    
    public String toString(){
        return("" + Double.toString(Overdraft) + "" + Conditions + "" + Double.toString(AvailableBalance) + "\n");
    }
    
    public String searchByFirstName() {
        return this.PrimaryFirstName;
    }
    
    public String searchBySurname (){
        return this.PrimarySurname;
    }
    
    public Account getAccountDetails(){
        return this.AccountDetails;
    }
    
    public double getOverdraft(){
        return Overdraft;
    }
    
    public String getConditions(){
        return Conditions;
    }
    
    public double getAvailableBalance (){
        return AvailableBalance;
    }
    
    public void LoadFromFile (BufferedReader bin){
    
        try {
                AccountDetails.LoadFromFile(bin);
                Overdraft = Double.parseDouble(bin.readLine());
                Conditions = bin.readLine();
                AvailableBalance = Double.parseDouble(bin.readLine());
                bin.close();
        }catch(IOException ioe){}
    }
    
    public void Deposit(Double inAmount){
    
        System.out.println("Old Balance = " + this.Balance);
        super.Deposit(inAmount);
    }
    
    public void Withdraw (Double inAmount){
        double OverdraftCheck = (Balance + Overdraft) - inAmount;
        
        if(OverdraftCheck>0){
                    
            super.Withdraw(inAmount);
            
        }
        else {
            super.Withdraw(inAmount - 25);
            
        }
    
    }
    
    

    public String Display(JTextArea src) { // inside of inherritance use the same names and utilize overriding and overloading insead of new names it shortenes the program code 
        src.setLineWrap(true);
        src.append(super.Display() + "Overdraft - " + Overdraft + "\n");
        return null;
    }

    
    
    
    
}
