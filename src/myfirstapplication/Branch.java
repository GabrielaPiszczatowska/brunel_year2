/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myfirstapplication;


import java.util.Locale;
import javax.swing.*;
import java.awt.*;
import static java.awt.image.ImageObserver.ERROR;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.NoSuchElementException;
import java.util.Scanner;
/**
 *
 * @author Gabrysia
 */

public class Branch {
    
    private String BranchName;
     private IAddress mailingAddress;
     private String workingHours;
     private String sortCode;
     private boolean HeadOffice;
     private Person manager;
     FileWriter writer;
     
 IAddress theAddress; 
     
 public Branch(String line){
     
     String[] split = line.split(",");
     BranchName = split[0] ; 
     mailingAddress = new IAddress(Integer.parseInt(split[1]),split[2],split[3],split[4],split[5],split[6],split[7]);
     workingHours = split[8];
     sortCode = split[9];
     HeadOffice = false;
 
 }
     
 public Branch (){
     this.Edit("", new IAddress(), "", "", false);//instantiates the object of IAddress right away, HeadOffice is not by default the HeadOffice Branch
     
     
//     mailingAddress = ; // instancja, nie moge kozystac ze zmiennych bo sa prywatne ale za to metody moga byc uzywane bo sa publiczne
     
 }  
 
 public Branch (String BranchName, IAddress src, String workingHours, String sourceCode, boolean HeadOffice){
     this.Edit( BranchName, src, sourceCode, workingHours, HeadOffice);
 }
 
 public Branch(BufferedReader bin){
     
     mailingAddress = new IAddress();
 
 }
 public void Edit(String BranchName, IAddress src, String workingHours, String sortCode, boolean HeadOffice){
 
     this.BranchName = BranchName;
     this.mailingAddress = src;
     this.workingHours = workingHours;
     this.sortCode = sortCode;
     this.HeadOffice = HeadOffice;
     
    // this.manager = manager;
 }
 public String Display (JTextArea src){
     
     String str = BranchName + "\n" + mailingAddress.HeadOfficetoString() + "\n" + workingHours + "\n" + sortCode + "\n";
     
   return str;
     
     
 }
 
 public String toString(){
     return BranchName + "," + mailingAddress.toString() + "," + workingHours + "," + sortCode + System.getProperty("line.separator");
     
 }
 
 public String HeadOtoString(){
     return(BranchName + "\n" + mailingAddress.HeadOfficetoString() + "\n" + workingHours + "\n" + sortCode);
 }
 
 
 public boolean IsHeadOffice(){
     return HeadOffice;
 }
 
 public boolean StringtoBool(String string) throws ParseException{
 
     boolean HeadOffice = Boolean.parseBoolean(string);
     return HeadOffice;
 
 }
 
 //UNCOMMENT?
// public void SaveToFile(FileWriter writer) throws IOException{
//     
//     try {
//     
//         writer.write(toString());
//         writer.flush();
////         writer.close();
//     }
//     catch (IOException ioe){
//         System.out.println("Cannot Save Head Ofiice Details");
//     }
//
// }
 
 public void SaveToFile(FileWriter writer){
    
    try {
            writer.write(toString());
            
            writer.flush();
        } catch (IOException ioe){
            System.out.println("Cannot save");
        }

}
 
 
   
 //FOR HEAD OFFICE DETAILS
 
// public void LoadFromFile(BufferedReader bin){
//     
//     try{
//         BranchName = bin.readLine();
//         mailingAddress.LoadFromFile(bin);
//         workingHours = bin.readLine();
//         sortCode = bin.readLine();
//     
//     }catch(IOException ioe){}
 
 
 //}
 
 public void LoadFromFile (JTextArea src){ 
  String filename = "textFieldOutput.txt";
     Scanner reader = null;

       try
       {
         reader = new Scanner(new File(filename));
       }
       catch (FileNotFoundException e)
       {
         System.out.println("Cannot open file " + filename);
         return;
       }
       src.setText("");//wyczysc wszystko co tu bylo zeby tego nie nadpisywac
       while(reader.hasNextLine())
       {
         String line = reader.nextLine();//to sie bedzie czytac dopoki sa linijki, jak dojdzie do ostatniej linijki to nie bedzie dalej czytac
         System.out.println(line + "\n");//drukuje w konsoli aktualnie czytana linie zeby wiedziec ze cos sie robi
         
         src.append(line);//dodaj ta linie do miejsca wyswietlania
         src.append("\n");//przejscie do nowewj lini
       }
       reader.close();

 }
 
 //FOR BRANCH LIST
 
 public void initialize(){
     Branch aBranch = new Branch();
     try{
     
         Scanner sc = new Scanner(new File ("BranchList.txt"));
         System.out.println("Scanning in branch list has started");
            while (sc.hasNext()){
                String line = sc.next();
                String[] index = line.split(",");
                IAddress aToken = new IAddress(Integer.parseInt(index[1]),index[2].trim(), index[3].trim(), index[4].trim(),index[5].trim(),index[6].trim(),index[7].trim());
                Branch bToken = new Branch(index[8].trim(), aToken, index[9].trim(),index[10].trim(), Boolean.parseBoolean(index[11].trim()));
                
                aBranch = bToken;
            }
     } catch (FileNotFoundException ex){}
     
//     return aBranch;
 
 }
 
  
 
 
// 
 public void SaveToFile(){     // Save To File For Head Office
        String Output = "";
        try{
         String fileName = ("textFieldOutput.txt");
         File actualFile = new File (fileName); 
           writer = new FileWriter(actualFile);
           Output = this.toString();
           //Output = this.toString().substring(0, this.toString().length() - 1);     // Used to remove @ symbol, avoids rewriting toString method
           writer.write(Output + System.getProperty("line.separator"));
           writer.flush();
           writer.close();
        }catch(IOException ioe){
            
        }  
        
    }

   public String getBranchName(){
       return BranchName;
   }
   
    public IAddress getBranchAddress() {
        return mailingAddress;
    }
    public String getworkingHours(){
        return workingHours;
    }
    
    public String getsortCode(){
        return sortCode;
    }
    
    
 

}


 
 // two constructors present, default constr and constr w/parameters       
//
//    void SaveToFile(String textFieldOutputtxt) throws IOException {
//              FileWriter filee = new FileWriter("textFieldOutput.txt", false);
//            filee.write(this.toString());
//            filee.flush();
//            filee.close();
//            
//}}
