/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myfirstapplication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

/**
 *
 * @author Gabrysia
 */
public class BranchList {
    
    private ArrayList <Branch> Branches;
    private String BranchList = "BranchList.txt";
    private BranchList subDepartaments;
    
    
    public BranchList(){
        Branches = new ArrayList <Branch>();
        subDepartaments = null; //set as null so it will not create an infinite loop, otherwise there will be a continuous error message
    }
    public void setSubDepartments(BranchList list) {
        subDepartaments = list;
    }
    
    public void addBranch(Branch src){
        Branches.add(src);
    }
    
    public void removeBranch (Branch src){
        Branches.remove(src);
    }
    
    public void removeBranch(String BranchName){
    
        for (int i = 0; i<Branches.size(); i++){
            
            if(Branches.get(i).getBranchName().equalsIgnoreCase(BranchName)){
                Branches.remove(i);
            }
        }
        System.out.println(Branches.size());
    }
    
    
    public void Display(JTextArea jBranchTextArea){

       System.out.println("Number of Branches" + Integer.toString(Branches.size()));
      jBranchTextArea.setText("");
      jBranchTextArea.setLineWrap(true);
      String displayText = "";
      for(int i = 0; i < Branches.size(); i++){
          
         
         displayText =  displayText.concat(Branches.get(i).Display(jBranchTextArea));//appending concat=join
      }
      jBranchTextArea.setText(displayText);
    
    }
    
    public void Initialize(){
    
        try{
            Scanner sc = new Scanner(new File ("BranchList.txt"));
            System.out.println("Branch List scanner initialized");
            sc.useDelimiter("@");
            
            while(sc.hasNext()){
                
                String line = sc.next();
                String[] index = line.split(",");
                
                IAddress aToken = new IAddress(Integer.parseInt(index[1]),index[2].trim(), index[3].trim(), index[4].trim(),index[5].trim(),index[6].trim(), index[7].trim());
                Branch bToken = new Branch(index[8].trim(), aToken, index[9].trim(),index[10].trim(),Boolean.parseBoolean(index[11].trim()));
                Branches.add(bToken);
                
            }
                System.out.println("Delimeter:" +sc.delimiter());
                System.out.println("List Size: " + Branches.size()+"\n");
        }catch (FileNotFoundException ex){}
    
    }
    
    public void SavetoFile(){
        
        
          String Output = "";
      try {
            FileWriter fw = new FileWriter("BranchList.txt",false);
            PrintWriter pwOb = new PrintWriter(fw, false);
            pwOb.flush();
            pwOb.close();
            FileWriter file = new FileWriter(new File("BranchList.txt"),true);
            for (Branch src : Branches){
                
                src.SaveToFile(file);
            }
            fw.write(Output + System.getProperty("line.separator"));
            file.flush();
            file.close();
            System.out.println("Zapisuje sie");

        } catch (IOException ex) {

        }
        
       
//        try{
//            FileWriter writer = new FileWriter("BranchList.txt", false);
//            PrintWriter printwriter = new PrintWriter(writer, false);
//            printwriter.flush();
//            printwriter.close();
//            FileWriter file = new FileWriter(new File("BranchList.txt"),true);
//                
//                for(Branch src : Branches){
//                    src.SaveToFile(file);
//                }
//                file.flush();
//                file.close();
//            
//        }catch (IOException ioe){}
    
    }
    

    
    public void LoadFromFile() throws ParseException, FileNotFoundException, IOException{
        BufferedReader bin = new BufferedReader(new FileReader(BranchList));
        String line ;//= bin.readLine();
        Branches.clear();
        while ((line = bin.readLine())!=null)
        {
            Branch b = new Branch(line);
            Branches.add(b);
        }
    }
    
    
    //SELF ASSOCIATION
    

    
    

    
    
}
