/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myfirstapplication;

import java.util.Locale;
import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Gabrysia
 */
public class IAddress {
    
    private int HouseNo;
    private String Name;
    private String Street;
//    private String HouseName;
    private String PostCode;
    private String Town;
    private String Area;
    private String Country;
    private String fileaddress = "adress.txt";
    
    public IAddress (){
        
       this.Edit(0, "", "", "", "",  "", "");
        
    }
    
    public IAddress(int HouseNo, String Name, String Street, String Town, String Area,
         String Country, String PostCode){
        
       this.Edit(HouseNo, Name, Street,  Town, Area, Country, PostCode);
    
    }

  
    public int getHouseNo (){
        return HouseNo;
    }
    
    public String getName (){
        return Name;
    }
    
    public String getStreet (){
        return Street;
    }
    
    public String getPostCode (){
        return PostCode;
    }
    
    public String getTown(){
        return Town;
    }
    
    public String getCountry (){
        return Country;
    }
    
    

    
    
    public void Display (JTextArea src){
     src.append(this.toString());
    }
    
    public String toString(){
        return Integer.toString(HouseNo) + ","+ Name + "," + Street + "," + Town + ","+ Area + ","+ Country + "," + PostCode;
    }
    
    public String HeadOfficetoString(){
        return(Name + " " + HouseNo + " " + Street + " " + Town + " " + Area + " " + Country + " " + PostCode + " ");
    }
    
   
    
    public boolean SaveToFile(String newName, String newStreet, int newHouseNo, String newPostCode, String newTown, String newCountry,FileWriter writer ){
        boolean isSaved;
        Name = newName;
        Street = newStreet;
        HouseNo = newHouseNo;
//        HouseName = newHouseName;
        PostCode = newPostCode;
        Town = newTown;
        Country = newCountry;
        
//        FileWriter writer;
        try{
//            writer = new FileWriter (fileaddress, true);
            writer.write(Name+System.getProperty("line.separator"));
            writer.write(Street+System.getProperty("line.separator"));
            writer.write(HouseNo+System.getProperty("line.separator"));
//            writer.write(HouseName+System.getProperty("line.separator"));
            writer.write(PostCode+System.getProperty("line.separator"));
            writer.write(Town+System.getProperty("line.separator"));
            writer.write(Country+System.getProperty("line.separator"));
            writer.write("##" + System.getProperty("line.separator"));
            isSaved = true;
                writer.flush();
                writer.close();
                writer = null;
        }catch (IOException ioe) {
                isSaved = false;
            }
        
        return isSaved; 
    
    }
    
    public boolean LoadFromFile(BufferedReader bin, boolean isABranch ){

        boolean isFound = false;
        String record;
        FileReader reader;
        try{
            HouseNo = Integer.parseInt(bin.readLine());

            if (isABranch) {
                Name = bin.readLine();
            }
            Street = bin.readLine();
            Area = bin.readLine();
            
//            HouseName = bin.readLine();
            PostCode = bin.readLine();
            Town = bin.readLine();
            
            Country = bin.readLine();
            
        }catch (IOException ioe) {
            isFound = false;
        }
        return isFound;
    }
    
   public void Edit(int HouseNo, String Name, String Street, String Town, String Area,
         String Country, String PostCode){
       this.HouseNo = HouseNo;
       this.Name = Name;
       this.Street = Street;
//       this.HouseName = HouseName;
       this.Town = Town;
       this.Area = Area;
       this.Country = Country;
       this.PostCode = PostCode;
   }

 
    

}
