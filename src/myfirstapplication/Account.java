/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myfirstapplication;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JTextArea;

/**
 *
 * @author Gabrysia
 */
public abstract class Account {
    //abstract class cannot be instantiated, it serves as a base for the subclassess
    
    protected String SortCode; //only accessible fromthis superclasses subclasses 
    protected int AccountNumber;
    protected Double Balance; //hold floating point values
    protected String BankName;
    protected Double Rate;
    protected String LastReportDate;
    protected String PrimaryFirstName;
    protected String PrimarySurname;
    protected Boolean Joint;
    protected String AccountType;
    protected int NoTransactions;
    
     private Person AccountHolder = new Person();
    
    
    public Account(){
    
      SortCode = "";
      AccountNumber = 0;
      Balance = 0.0;
      BankName = "";
      Rate = 0.0;
      LastReportDate = "";
      PrimaryFirstName = "";
      PrimarySurname = "";
      Joint = false;
      AccountType = "Current";
      NoTransactions = 0;
 
    }
   
    public Account(String SortCode,int AccountNumber, Double Balance, String BankName, Double Rate, String LastReportDate, String PrimaryFirstName, String PrimarySurname, Boolean Joint, String AccountType, int NoTransactions){
        this.Edit(SortCode,AccountNumber,Balance,BankName,Rate,LastReportDate,PrimaryFirstName,PrimarySurname,Joint,AccountType,NoTransactions);
    }
    
    public void Edit(String SortCode,int AccountNumber, Double Balance, String BankName, Double Rate, String LastReportDate, String PrimaryFirstName, String PrimarySurname, Boolean Joint, String AccountType, int NoTransactions){
    
        this.SortCode = SortCode;
        this.AccountNumber = AccountNumber;
        this.Balance = Balance;
        this.BankName = BankName;
        this.Rate = Rate;
        this.LastReportDate = LastReportDate;
        this.PrimaryFirstName = PrimaryFirstName;
        this.PrimarySurname = PrimarySurname;
        this.Joint = Joint;
        this.AccountType = AccountType;
        this.NoTransactions = NoTransactions;
     
    }
    
    
    public void addAccHolder (Person AccountHolder){
        this.AccountHolder = AccountHolder;
    
    }
    
    public void Deposit (Double inAmount){
        Balance = Balance + inAmount;
    
    }
    
    public void Withdraw (Double inAmount){
        Balance = Balance - inAmount;
    
    }
    
    public double Charges (Double woCharg){
        double Output = woCharg * Rate;
        return Output;
        
    }
    
    
    public String toString(){
        return(SortCode + "" + Integer.toString(AccountNumber) + "" + Double.toString(Balance) + "" + BankName + "" + Double.toString(Rate) + "" + LastReportDate + "" + 
                PrimaryFirstName + "" + PrimarySurname + "" + Boolean.toString(Joint) + "" + AccountType + "" + Integer.toString(NoTransactions));
    }
    
 
    //
    public abstract String Display (JTextArea src);
    
    public String getFirstName () {
        return PrimaryFirstName;
    }
    
    public String getSurname (){
        return PrimarySurname;
    }
    
    public String getSortCode (){
        return SortCode;
    }
    
    public int getAccountNo(){
        return AccountNumber;
    }
    
    public double getBalance (){
        return Balance;
    }
    
    public String getBankName(){
        return BankName;
    }
    
    public double getRate (){
        return Rate;
    }
    
    public String getLasReportedDate (){
        return LastReportDate;
    }
    
    public Boolean getJoint(){
        return Joint;
    }
    
    public String getAccountType (){
        return AccountType;
    }
    
    public Integer getTransactions(){
        return NoTransactions;
    }
    
    public String Display(){
    
        return ("Account Holder " + getFirstName() + "" + getSurname() + "\n"
                + "Account Type " + getAccountType() + "\n"
                + "Sort Code" + getSortCode() + "\n"
                + "Account no " + getAccountNo() + "\n"
                + "Balance " + getBalance () +"\n"
                + "Bank Name " + getBankName() + "\n"
                + "Rate" + getRate() + "\n"
                + "Joint Account " + getJoint() +"\n");
    }
    
    public void SavetoFile(FileWriter writer){
        
        try {
                writer.write(toString());
                writer.flush();
                writer.close();
        } catch (IOException ioe){
                System.out.println("cannot save account details");
        } 
    
    }
    
    public void LoadFromFile (BufferedReader bin) {
     
        try {
            PrimaryFirstName = bin.readLine();
            PrimarySurname = bin.readLine();
            SortCode = bin.readLine();
            AccountNumber = Integer.parseInt(bin.readLine());
            Balance = Double.parseDouble(bin.readLine());
            BankName = bin.readLine();
            Rate = Double.parseDouble(bin.readLine());
            LastReportDate = bin.readLine();
            Joint = Boolean.parseBoolean(bin.readLine());
            AccountType = bin.readLine();
            NoTransactions = Integer.parseInt(bin.readLine());
        
        } catch (IOException ioe){}
    }
}


