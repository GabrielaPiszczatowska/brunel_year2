/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myfirstapplication;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JTextArea;

/**
 *
 * @author Gabrysia
 */
public class SavingAccount extends Account{

    
    private double WithdrawLimit;
    private boolean Days;
    private boolean rateDecrease;
    
    private Account AccountDetails;
    
    public SavingAccount(){
    
        WithdrawLimit = 200;
    }
    
    public SavingAccount(String SavingSortCode, Integer SavingAccountNumber, Double SavingBalance, String SavingBankName, Double SavingRate, String SavingLastReportDate, String PrimaryFirstName, String PrimarySurname, Boolean SavingJoint, String SavingAccountType, Integer SavingNoTransactions, Double SavingWithdrawLimit){
    super(SavingSortCode, SavingAccountNumber, SavingBalance, SavingBankName, SavingRate, SavingLastReportDate, PrimaryFirstName, PrimarySurname, SavingJoint, SavingAccountType, SavingNoTransactions);
    WithdrawLimit = SavingWithdrawLimit;
    Days = true;
    rateDecrease = false;
    
    }
    
    public double savingsCharges(Double woCharg){
        return super.Charges(woCharg);
    
    }

      public String Display(JTextArea src) {
        src.setLineWrap(true);
        src.append(super.Display() + "Withdraw Limit" + WithdrawLimit + "\n");
        
        return null;
    }
      
      public String savingToString(){
          return(" " + Double.toString(WithdrawLimit) + "\n");
      }
      
      public String searchFirstName(){
      return this.PrimaryFirstName;
      }
      
      public String searchSurname(){
      return this.PrimarySurname;
      }
      
      public Account getAccountDetails(){
          return this.AccountDetails;
      }
      
      public double getWithdrawLimit(){
          return WithdrawLimit;
      }
      
      
      public void SavetoFile(FileWriter writer){
      
          try{
              writer.write(super.toString());
              writer.write(savingToString());
    
          } catch(IOException ioe){
          System.out.println("cannot save saving account details");
          }
      }
      
      public void LoadFromFile(BufferedReader bin){
      
          try{
                AccountDetails.LoadFromFile(bin);
                WithdrawLimit = Double.parseDouble(bin.readLine());
                bin.close();
          } catch (IOException ioe){}
      }
      
     
      public void savingDeposit(Double inAmount){
      
          System.out.println("Old Balance = " + this.Balance);
          super.Deposit(inAmount);
          
      }
      
      public void savingWithdraw(Double inAmount){
      
          if (inAmount < WithdrawLimit && Days == true){
              
              super.Withdraw(inAmount - 10);
              
          }
          
          else {
           
              super.Withdraw(inAmount);
          }
      }
    
    
    
}
