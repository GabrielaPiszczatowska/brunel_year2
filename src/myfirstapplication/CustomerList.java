/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myfirstapplication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

/**
 *
 * @author Gabrysia
 */
public class CustomerList {
 
    ArrayList <Person> Clients; //declaration of array of objects
    private String CustomerList = "CustomerList.txt";
    
    public CustomerList () {
        Clients = new ArrayList <Person> ();
                try {
            LoadFromFile(); //adds to existing list without displaying, uses memory instead relying heavily on the files, previously it worked only on files and not objects
        } catch (IOException ex) {
            Logger.getLogger(CustomerList.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(CustomerList.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    
    public void FindAndDisplay(String FirstName, JTextArea jPersonTextArea){
        jPersonTextArea.setText("");
        
        for(int i = 0; i < Clients.size(); i++){
            
      if (Clients.get(i).getFirstName().equalsIgnoreCase(FirstName)){
      
            jPersonTextArea.setText(Clients.get(i).Display());//we pass the strings back 
        
      }
    }
    }
    
    
    public void Display (JTextArea jPersonTextArea)  {
     
       System.out.println("Number of clients" + Integer.toString(Clients.size()));
      jPersonTextArea.setText("");
      jPersonTextArea.setLineWrap(true);
      String displayText = "";
      for(int i = 0; i < Clients.size(); i++){
          
         
         displayText =  displayText.concat(Clients.get(i).Display());//appending concat=join
      }
      jPersonTextArea.setText(displayText);        
    }
   
    public void SaveToFile () {

            
        String Output = "";
      try {
            FileWriter fw = new FileWriter("CustomerList.txt",false);
            PrintWriter pwOb = new PrintWriter(fw, false);
            pwOb.flush();
            pwOb.close();
            FileWriter file = new FileWriter(new File("CustomerList.txt"),true);
            for (Person src : Clients){
                
                src.SaveToFile(file);
            }
            fw.write(Output + System.getProperty("line.separator"));
            file.flush();
            file.close();
            System.out.println("Zapisuje sie");

        } catch (IOException ex) {

        }
       
        
    }
    
       
  
    
    public void LoadFromFile() throws ParseException, FileNotFoundException, IOException{
        BufferedReader bin = new BufferedReader(new FileReader(CustomerList));
        String line ;//= bin.readLine();
        Clients.clear();
        while ((line = bin.readLine())!=null)
        {
            Person p= new Person(line);
            Clients.add(p);
        }
    }
    
    
    
    public void add (Person src){
        Clients.add(src);
    
    }        
     
    public void DeleteCustomer(Person src) {
        Clients.remove(src);
    
    }
    
   public void remove(String FirstName, String Surname){
        for (int i = 0; i < Clients.size(); i++){
            if ((Clients.get(i).getSurname().equalsIgnoreCase(Surname)) && (Clients.get(i).getFirstName().equalsIgnoreCase(FirstName))){
                Clients.remove(i);
            }
        }
        System.out.println(Clients.size());
    }

    public void initialization() throws ParseException{
    
    try{
        
        Scanner sc = new Scanner(new File("CustomerList.txt"));
        System.out.println("scanning customerlist is working");
        
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        
        sc.useDelimiter("\n");
        
            while(sc.hasNext()){
                String line = sc.next();
                String[] index = line.split(",");
                
                IAddress addressarray = new IAddress(Integer.parseInt(index[2]), index[3],index[4],index[5],index[6],index[7],index[8]);
                Person personarray = new Person(index[0],index[1],addressarray, df.parse(index[9]), df.parse(index[10]));
               Clients.add(personarray);
                
                
            }
            System.out.println("Delimiter:" + sc.delimiter());
            System.out.println("List Size:" + Clients.size() + "\n");
            
    
    }catch(FileNotFoundException ex){}
    }
    

}
