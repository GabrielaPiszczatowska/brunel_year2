/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myfirstapplication;

/**
 *
 * @author Gabrysia
 */
public class MyFirstApplication {

    /**
     * @param args the command line arguments
     */

    
    public static double sum(double input1, double input2, double input3){
        try{
            double result = input1+input2+input3;
            return result;
        }
        catch(java.lang.NumberFormatException e){
            return 0;
        }
        }
        
    
    
    public static double subtraction(double input1, double input2, double input3){
        return input1-input2-input3;
    }
    
    public static double multiplication(double input1, double input2, double input3){
        return input1*input2*input3;
    }
    
    public static double division(double input1, double input2){
        return input1/input2;
    }
    
    public static double power(double base, int power) {
        double result = 1;

        if (power < 0) {
            base = 1.0 / base;
            power = -power;
        }

        for (int  i = 0; i < power; i++) {
            result = result * base;
        }

        return result;
    }
    
    public static double rectangularVolume(double height, double width, double lenght){
        return height*width*lenght;
    }
    
    public static double cylinderVolume(double radius, double height){
        return 3.14d*radius*radius*height;
    }
    
    public static double coneVolume(double radius, double height){
        return 1d/3d*3.14*radius*radius*height;
    }
    
    public static double squareBasedPyramidVolume(double area, double height){
        return 1d/3d*area*height;
    }
    
}
