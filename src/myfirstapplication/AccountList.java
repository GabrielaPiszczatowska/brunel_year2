/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myfirstapplication;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JTextArea;

/**
 *
 * @author Gabrysia
 */
public class AccountList {
    
    
    public ArrayList <Account> AccountsList;
    private String AccountFile = "Accounts.txt";
    public double newBalance;
    private Boolean ApplyWithdrawalFee = false;
    public int currentAccountSize;
    
    public AccountList(){
    
        AccountsList = new ArrayList <Account> ();
        
    }
    
    public void addAccount(Account src){
        AccountsList.add(src);
    }
    
    public void removeAccount(Account src){
        AccountsList.remove(src);
    }
    
    public void removeAccount(String FirstName, String Surname){
        
        for (int i = 0; i < AccountsList.size(); i++){
        
            if((AccountsList.get(i).getSurname().equalsIgnoreCase(Surname)) && (AccountsList.get(i).getFirstName().equalsIgnoreCase(FirstName))){
                AccountsList.remove(i);
            
            }
        }
        System.out.println(AccountsList.size());
    }
    
    public void Display (String AccountType, String FirstName, String Surname, JTextArea jShowAccountsTextArea){
        
        jShowAccountsTextArea.setText("");
        
        for (int i = 0; i < AccountsList.size(); i++){
            if(AccountsList.get(i).getFirstName().equalsIgnoreCase(FirstName) && AccountsList.get(i).getSurname().equalsIgnoreCase(Surname) && AccountsList.get(i).getAccountType().equalsIgnoreCase(AccountType)){
            AccountsList.get(i).Display(jShowAccountsTextArea);
            }
        }

    }
    
    public void FileInitialization(){
    
        try{
                Scanner sc = new Scanner(new File ("Accounts.txt"));
                System.out.println("account scanning has started");
                sc.useDelimiter("\n");
                
                    while(sc.hasNext()){
                    
                        String line = sc.next();
                        String [] index = line.split("");
                        
                                if("Currnet".equalsIgnoreCase(index[9])){
                                CurrentAccount cToken = new CurrentAccount (index[0].trim(), Integer.parseInt(index[1]), Double.parseDouble(index[2]), index[3].trim(), Double.parseDouble(index[4]), index[5].trim(),
                                                                            index[6].trim(), index [7].trim(), Boolean.parseBoolean(index[8]), index[9].trim(), Integer.parseInt(index[10]), Double.parseDouble(index[11]), 
                                                                            index[12], Double.parseDouble(index[13]));
                                AccountsList.add(cToken);
                                }
                                else if("ISA".equalsIgnoreCase(index[9])){
                                ISAAccount isaToken = new ISAAccount(index[0].trim(), Integer.parseInt(index[1]), Double.parseDouble(index[2]), index[3].trim(), Double.parseDouble(index[4]), index[5].trim(),
                                                                            index[6].trim(), index [7].trim(), Boolean.parseBoolean(index[8]), index[9].trim(), Integer.parseInt(index[10]), Double.parseDouble(index[11]), 
                                                                            Double.parseDouble(index[12]));
                                AccountsList.add(isaToken);
                                }
                                else if ("Savings".equalsIgnoreCase(index[9])){
                                SavingAccount sToken = new SavingAccount(index[0].trim(), Integer.parseInt(index[1]), Double.parseDouble(index[2]), index[3].trim(), Double.parseDouble(index[4]), index[5].trim(),
                                                                            index[6].trim(), index [7].trim(), Boolean.parseBoolean(index[8]), index[9].trim(), Integer.parseInt(index[10]), Double.parseDouble(index[11]));
                                AccountsList.add(sToken);
                                }
                                
                    }
                    System.out.println("Delimiter: " + sc.delimiter());
                    System.out.println("List Size: " + AccountsList.size() + "\n");
                
        }catch (FileNotFoundException ex){}
    }
    
    public void AccountSavetoFile(){
       try{
            FileWriter writer = new FileWriter("Accounts.txt", false);
            PrintWriter printwriter = new PrintWriter (writer, false);
            printwriter.flush();
            printwriter.close();
            
       }catch (IOException ioe){
       
           System.out.println("cannot save account details to accountlist");
       }
    }
    
    //USED IN WITHDRAW AND DEPOSIT BUTTONS
    public void Deposit(Double inAmount, int AccountNo){
        
        for(int k=0; k < AccountsList.size(); k++){
        
            if(AccountsList.get(k).getAccountNo() == AccountNo){
                AccountsList.get(k).Deposit(inAmount);
                AccountSavetoFile();
            
            }
        }
    
    
    }
    
    public void Withdraw (Double inAmount, int AccountNo){
        
        for(int k=0; k < AccountsList.size(); k++){
        
            if(AccountsList.get(k).getAccountNo() == AccountNo){
                AccountsList.get(k).Withdraw(inAmount);
                AccountSavetoFile();
            
            }
        }
    
    }
    
}
