/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myfirstapplication;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JTextArea;

/**
 *
 * @author Gabrysia
 */
public class ISAAccount extends Account {
    
    
    private double YearlyLimit;
    private double ThisYearsDeposit;
   
    private Account AccountDetails;
    
    public ISAAccount(){
        YearlyLimit = 3250;
        ThisYearsDeposit = 0;
    }
    
    public ISAAccount (String ISASortCode, Integer ISAccountNumber, Double ISABalance, String ISABankName, Double ISARate, String ISALastReportDate, String PrimaryFirstName, String PrimarySurname, Boolean ISAJoint, String ISAccountType, Integer ISANoTransactions, Double YearlyLimit, Double ThisYearsDeposit ){
    
        super (ISASortCode, ISAccountNumber, ISABalance, ISABankName, ISARate, ISALastReportDate, PrimaryFirstName,PrimarySurname, ISAJoint, ISAccountType, ISANoTransactions);
        this.YearlyLimit = YearlyLimit;
        this.ThisYearsDeposit = ThisYearsDeposit;
        
    }
    
    private String isatoString(){
        return ( PrimaryFirstName + "" + PrimarySurname + "" 
                 + SortCode + "" + Integer.toString(AccountNumber) + "" + Double.toString(Balance) + ""
                 + BankName + "" + Double.toString(Rate) + "" + Boolean.toString(Joint) + ""
                + super.LastReportDate + "" + AccountType + "" + NoTransactions);
    
    }
    
    public double isaCharges (Double woCharg){
        return super.Charges(woCharg);
    }
    
    public String isaWithdraw (Double woCharg){
    
        String Output = " cannot withdraw from isaaccount atm \n";
        return Output;
    }
    

    public String Display(JTextArea src) {
        
        src.setLineWrap (true);
        src.append(super.Display()
        + "Yearly Limit " + YearlyLimit + "\n"
        + "This Yaers Deposits" + ThisYearsDeposit + "\n");
       
        return null;
    }
    
    public String iToString(){
        return(" " + Double.toString(YearlyLimit) + " " + Double.toString(ThisYearsDeposit) + "\n");
    }
    
    //FINDING USER
    
    public String SearchFirstName(){
        return this.PrimaryFirstName;
    }
    
    public String SearchSurname(){
        return this.PrimarySurname;
    }
    
    
    public Account getAccountDetails(){
        return this.AccountDetails;
    }
    
    public double getYearlyLimit(){
        return this.YearlyLimit;
    }
    
    public double getThisYearsDeposit(){
        return this.ThisYearsDeposit;
    }
    
    
    
    public void SavetoFile(FileWriter writer){
    
        try{
            writer.write(isatoString());
            writer.write(iToString());   
        }catch (IOException ioe){
        System.out.println("cannot save isa account details");
        }
    }
    
    public void LoadFromFile(BufferedReader bin){
        
        try{
            AccountDetails.LoadFromFile(bin);
            YearlyLimit = Double.parseDouble(bin.readLine());
            ThisYearsDeposit = Double.parseDouble(bin.readLine());
            bin.close();
        
        }catch(IOException ioe){}
    
    }
    
    public void Deposit(Double inAmount){
        
        Double Input = inAmount + ThisYearsDeposit;
        
        if(Input <= YearlyLimit){
            
            ThisYearsDeposit = ThisYearsDeposit + inAmount;
            super.Deposit(inAmount);
            
        }
    
    }
}
